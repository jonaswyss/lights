

class Color:
    def __init__(self, r: int = 0, g: int = 0, b: int = 0):
        self.r = r
        self.g = g
        self.b = b

    def __eq__(self, __o: object) -> bool:
        if isinstance(__o, Color):
            return self.r == __o.r and self.g == __o.g and self.b == __o.b
        else:
            return False


RED = Color(255, 0, 0)
GREEN = Color(0, 255, 0)
BLUE = Color(0, 0, 255)
