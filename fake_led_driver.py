import sys
from time import sleep
from typing import List
from color import *


class FakeLedDriver:
    def set(self, values: List[Color]):
        sys.stdout.flush()
        msg = "\r"
        for i, v in enumerate(values):
            msg += self._get_color_escape(v.r, v.g, v.b)
            msg += str(i)
        sys.stdout.write(msg)

    def _get_color_escape(self, r, g, b, background=False):
        return '\033[{};2;{};{};{}m'.format(48 if background else 38, r, g, b)


if __name__ == "__main__":
    while True:
        FakeLedDriver().set([RED, BLUE, GREEN])
        sleep(0.1)
        FakeLedDriver().set([GREEN, RED,  BLUE])
        sleep(0.1)
        FakeLedDriver().set([BLUE, BLUE,  BLUE])
        sleep(0.1)
