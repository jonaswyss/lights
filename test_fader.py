import unittest
from fader import Fader
import color

START_COLOR = color.Color(0,0,0)
END_COLOR = color.Color(3,3,3)

class TestFader(unittest.TestCase):

    def test_given_fader_returns_start_color_on_first_get(self):
        testee = Fader(START_COLOR, START_COLOR, 0)
        result = testee.get_next_color()
        self.assertEqual(START_COLOR, result)

    def test_given_start_and_end_colors_then_fades_correctly(self):
        testee = Fader(START_COLOR, END_COLOR, 3)

        result = testee.get_next_color()
        self.assertEqual(result, START_COLOR)

        result = testee.get_next_color()
        self.assertEqual(result, color.Color(1,1,1))

        result = testee.get_next_color()
        self.assertEqual(result, color.Color(2,2,2))

        result = testee.get_next_color()
        self.assertEqual(result, color.Color(3,3,3))

    def test_given_only_changes_in_single_channel_fades_correctly(self):
        testee = Fader(color.Color(5,0,0), color.Color(30,0,0), 5)

        result = testee.get_next_color()
        self.assertEqual(result, color.Color(5,0,0))

        result = testee.get_next_color()
        self.assertEqual(result, color.Color(10,0,0))

        result = testee.get_next_color()
        self.assertEqual(result, color.Color(15,0,0))

        result = testee.get_next_color()
        self.assertEqual(result, color.Color(20,0,0))
        result = testee.get_next_color()
        result = testee.get_next_color()
        result = testee.get_next_color()

        self.assertEqual(result, color.Color(30,0,0))

if __name__ == '__main__':
    unittest.main()
