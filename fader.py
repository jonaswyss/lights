from color import Color


class Fader:
    def __init__(self, start: Color, end: Color, iterations: int):
        self._start = start
        self._current_iteration = 0
        self._iterations = iterations
        if iterations == 0:
            self._perStepR = 0.0
            self._perStepG = 0.0
            self._perStepB = 0.0
        else:
            self._perStepR = (end.r - start.r) / iterations
            self._perStepG = (end.g - start.g) / iterations
            self._perStepB = (end.b - start.b) / iterations

    def get_next_color(self) -> Color:
        r = self._start.r + int(self._current_iteration * self._perStepR)
        g = self._start.g +int(self._current_iteration * self._perStepG)
        b = self._start.b +int(self._current_iteration * self._perStepB)
        if self._current_iteration < self._iterations:
            self._current_iteration += 1
        return Color(r, g, b)
