from typing import List
from rgb_schedule import RgbSchedule
from color import Color

class FadeSchedule(RgbSchedule):
    def __init__(self, start:Color, end:Color):
        self._start = start
        self._current = start
        self._end = end


    def get_next_values(self) -> List[int]:
        return super().get_next_values()