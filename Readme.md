# Structure

## Microservices
- Dmx-service
- RGB-service
- Webserver
- frontend
- sound service

## Monolith
- one server with api
- client
- How to sync?


## Controls
- Set Colors
- Set Programs
- Manual Access To everything

### DMX
- Either send once 
- Or send based on timer

### For Testabilty:
- component that creates programs has to be testabel

## Programming
- Keyframes
- Fade between 
- Timed by timer or music


## MVP
- API
- Webclient
- Set Color
- Set DMX Commands


Client --> API --> DMX