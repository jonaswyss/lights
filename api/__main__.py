from fastapi import FastAPI


app = FastAPI()

@app.get("/test")
def test():
    return "Hello World!"


if __name__ == "__main__":
    import os
    import uvicorn

    uvicorn.run(app)